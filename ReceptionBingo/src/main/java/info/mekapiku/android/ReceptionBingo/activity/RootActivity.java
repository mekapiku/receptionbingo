package info.mekapiku.android.ReceptionBingo.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.viewpager.StartViewPager;

public class RootActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_layout);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, StartViewPager.createInstance())
                .commit();
    }
}
