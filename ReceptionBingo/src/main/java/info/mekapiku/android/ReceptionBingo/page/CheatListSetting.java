package info.mekapiku.android.ReceptionBingo.page;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.bean.BingoData;
import info.mekapiku.android.ReceptionBingo.viewpager.BingoViewPager;

/**
 * Created by mitsuyasu on 13/10/17.
 */
public class CheatListSetting extends Fragment {

    private List<BingoData> cheatList;

    public CheatListSetting() {
    }

    public static Fragment createInstance() {
        return new CheatListSetting();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cheat_list_setting, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            initList();
        }

        final Button submitButton = (Button) getActivity().findViewById(R.id.cheat_list_setting_submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, BingoViewPager.createInstance(cheatList))
                        .commit();
            }
        });

        final Button addButton = (Button) getActivity().findViewById(R.id.cheat_list_setting_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils dialogUtils = new DialogUtils();
                dialogUtils.show(getActivity().getSupportFragmentManager(), "inputDialog");
            }
        });
    }

    private void initList() {
        cheatList = new ArrayList<BingoData>();
        ListView listView = (ListView) getActivity().findViewById(R.id.cheat_list_setting_view);
        CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(), 0, cheatList);

        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ListView list = (ListView) parent;
                BingoData item = (BingoData) list.getItemAtPosition(position);
                CustomArrayAdapter adapter = (CustomArrayAdapter) list.getAdapter();

                adapter.remove(item);

                return false;
            }
        });
    }

    private void onInputDialog(Integer value) {
        if (value > 75 || value < 1) {
            Toast.makeText(getActivity(), getText(R.string.input_dialog_limit_value_error), Toast.LENGTH_LONG).show();
            return;
        }

        for (BingoData data : cheatList) {
            if (data.getBingoNo().equals(value)) {
                Toast.makeText(getActivity(), getText(R.string.input_dialog_dup_error), Toast.LENGTH_LONG).show();
                return;
            }
        }
        cheatList.add(new BingoData(null, value));
    }

    private class CustomArrayAdapter extends ArrayAdapter<BingoData> {

        private LayoutInflater layoutInflater_;

        public CustomArrayAdapter(Context context, int textViewResourceId, List<BingoData> objects) {
            super(context, textViewResourceId, objects);
            layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public CustomArrayAdapter(Context context, int resource) {
            super(context, resource);
            layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BingoData item = (BingoData) getItem(position);

            if (null == convertView) {
                convertView = layoutInflater_.inflate(R.layout.cheat_list_row, null);
            }

            TextView textView = (TextView) convertView.findViewById(R.id.cheat_row_label);
            textView.setText(item.getBingoNo().toString());

            return convertView;
        }
    }

    private class DialogUtils extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            LayoutInflater factory = LayoutInflater.from(getActivity());
            final View inputView = factory.inflate(R.layout.input_dialog_layout, null);
            final EditText editText = (EditText) inputView.findViewById(R.id.input_dialog_edittext);

            return new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.input_dialog_title)
                    .setView(inputView)
                    .setPositiveButton(getText(R.string.input_dialog_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            try {
                                Integer value = Integer.parseInt(editText.getText().toString());
                                onInputDialog(value);
                            } catch (NumberFormatException e) {
                                // TODO
                                Toast.makeText(getActivity(), getText(R.string.input_dialog_value_error), Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setNegativeButton(getText(R.string.input_dialog_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    })
                    .create();
        }
    }

}
