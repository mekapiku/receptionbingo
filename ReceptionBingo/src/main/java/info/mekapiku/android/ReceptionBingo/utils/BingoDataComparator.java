package info.mekapiku.android.ReceptionBingo.utils;

import java.util.Comparator;

import info.mekapiku.android.ReceptionBingo.bean.BingoData;

/**
 * Created by mitsuyasu on 13/10/07.
 */
public class BingoDataComparator implements Comparator<BingoData> {

    public int compare(BingoData a, BingoData b) {
        int no1 = a.getBingoOrder();
        int no2 = b.getBingoOrder();

        if (no1 > no2) {
            return 1;
        } else if (no1 == no2) {
            return 0;
        } else {
            return -1;
        }
    }
}
