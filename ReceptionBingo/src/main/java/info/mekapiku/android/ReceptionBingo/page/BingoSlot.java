package info.mekapiku.android.ReceptionBingo.page;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.bean.BingoData;
import info.mekapiku.android.ReceptionBingo.game.BingoGameMain;

/**
 * Created by mitsuyasu on 13/10/07.
 */
public class BingoSlot extends Fragment {

    private BingoGameMain gameMain;
    private List<BingoData> cheatList;

    public BingoSlot(List<BingoData> cheatList) {
        this.cheatList = cheatList;
    }

    public static Fragment createInstance(List<BingoData> cheatList) {
        return new BingoSlot(cheatList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bingo_slot, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Button startBingo = (Button) getActivity().findViewById(R.id.bingo_view_start_button);
        final TextView bingoNo = (TextView) getActivity().findViewById(R.id.bingo_slot_bingo_no);

        startBingo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer bingo = gameMain.getSlotResult();

                if (bingo == null) {
                    startBingo.setText(getText(R.string.bingo_slot_button_end_label));
                    startBingo.setEnabled(false);
                } else {
                    startBingo.setText(getText(R.string.bingo_slot_button_next_label));
                    bingoNo.setText(bingo.toString());
                }
            }
        });

        if (savedInstanceState == null) {
            initGame(getActivity());
        }
    }

    protected void initGame(Context context) {
        gameMain = BingoGameMain.get(context);
        gameMain.setCheatList(cheatList);
        gameMain.gameStart();
    }
}
