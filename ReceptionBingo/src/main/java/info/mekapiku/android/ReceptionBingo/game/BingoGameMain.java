package info.mekapiku.android.ReceptionBingo.game;

import android.content.Context;
import android.util.Log;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import info.mekapiku.android.ReceptionBingo.bean.BingoData;
import info.mekapiku.android.ReceptionBingo.page.HistoryList;

/**
 * Created by mitsuyasu on 13/10/06.
 */
public class BingoGameMain {

    private static BingoGameMain instance;
    private Context context;
    private List<BingoData> bingoList;
    private List<BingoData> cheatList;

    private BingoGameMain(Context context) {
        this.context = context;

        { // Initialize
            bingoList = new LinkedList<BingoData>();
            cheatList = null;
        }
    }

    public static BingoGameMain get(Context context) {
        if (instance == null) {
            instance = new BingoGameMain(context);
        }
        return instance;
    }

    private void initGame() {
        { // Init Normal Bingo
            bingoList.clear();
            for (int i = 1; i <= 75; i++) {
                bingoList.add(new BingoData(null, Integer.valueOf(i)));
            }
            Collections.shuffle(bingoList);
        }

        { // Reception!!
            if (cheatList != null) {
                // Sort By BingoOrder
                // Collections.sort(cheatList, new BingoDataComparator());
                Collections.reverse(cheatList);

                for (BingoData cheat : cheatList) {
                    for (int i = 0; i < bingoList.size(); i++) {
                        if (cheat.getBingoNo().equals(bingoList.get(i).getBingoNo())) {
                            bingoList.remove(i);
                        }
                    }
                }

                for (BingoData cheat : cheatList) {
                    bingoList.add(0, cheat);
                }
            }
        }

        Log.d(this.getClass().getSimpleName(), "Initialized Game");
    }

    public void gameStart() {
        initGame();
    }

    public Integer getSlotResult() {
        if (!bingoList.isEmpty()) {
            BingoData data = bingoList.remove(0);
            HistoryList.addItem(data);
            return data.getBingoNo();
        }
        return null;
    }

    public void pause() {

    }

    public void resume() {

    }

    public void stop() {

    }

    public void setCheatList(List<BingoData> cheatList) {
        this.cheatList = cheatList;
    }
}
