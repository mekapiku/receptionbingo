package info.mekapiku.android.ReceptionBingo.bean;

/**
 * Created by mitsuyasu on 13/10/06.
 */
public class BingoData {
    private Integer bingoOrder;
    private Integer bingoNo;

    public BingoData(Integer bingoOrder, Integer bingoNo) {
        this.bingoOrder = bingoOrder;
        this.bingoNo = bingoNo;
    }

    public Integer getBingoOrder() {
        return bingoOrder;
    }

    public Integer getBingoNo() {
        return bingoNo;
    }
}
