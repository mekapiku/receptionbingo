package info.mekapiku.android.ReceptionBingo.page;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.bean.BingoData;

/**
 * Created by mitsuyasu on 13/10/18.
 */
public class ReceptionBingoStart extends Fragment {

    public ReceptionBingoStart() {

    }

    public static Fragment createInstance() {
        return new ReceptionBingoStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reception_bingo_start_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO
        final List<BingoData> cheatList = new ArrayList<BingoData>();
        cheatList.add(new BingoData(0, 10));
        cheatList.add(new BingoData(1, 11));
        cheatList.add(new BingoData(2, 12));
        cheatList.add(new BingoData(2, 12));

        final Button startButton = (Button) getActivity().findViewById(R.id.reception_bingo_start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, CheatListSetting.createInstance())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
