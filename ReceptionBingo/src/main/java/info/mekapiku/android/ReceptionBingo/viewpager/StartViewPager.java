package info.mekapiku.android.ReceptionBingo.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.page.NormalBingoStart;
import info.mekapiku.android.ReceptionBingo.page.ReceptionBingoStart;

/**
 * Created by mitsuyasu on 13/10/18.
 */
public class StartViewPager extends Fragment {

    private MyPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    public StartViewPager() {

    }

    public static Fragment createInstance() {
        return new StartViewPager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_viewpager_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPagerAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager) getActivity().findViewById(R.id.start_view_pager);
        mViewPager.setAdapter(mPagerAdapter);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final static int PAGE_SIZE = 2;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_SIZE;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return NormalBingoStart.createInstance();
                case 1:
                    return ReceptionBingoStart.createInstance();
                default:
                    break;
            }
            return null;
        }
    }
}
