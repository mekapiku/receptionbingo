package info.mekapiku.android.ReceptionBingo.page;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.viewpager.BingoViewPager;

/**
 * Created by mitsuyasu on 13/10/18.
 */
public class NormalBingoStart extends Fragment {

    public NormalBingoStart() {

    }

    public static Fragment createInstance() {
        return new NormalBingoStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.normal_bingo_start_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Button startButton = (Button) getActivity().findViewById(R.id.normal_bingo_start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, BingoViewPager.createInstance(null))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
