package info.mekapiku.android.ReceptionBingo.page;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.bean.BingoData;

/**
 * Created by mitsuyasu on 13/10/06.
 */
public class HistoryList extends Fragment {

    private static ArrayAdapter<Integer> adapter;

    public HistoryList() {
    }

    public static Fragment createInstance() {
        return new HistoryList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            initList(getActivity());
        }
    }

    private void initList(Context context) {

        ListView listView = (ListView) ((Activity) context).findViewById(R.id.history_list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        adapter = new ArrayAdapter<Integer>(context, android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
    }

    public static void addItem(BingoData bingoData) {
        adapter.insert(bingoData.getBingoNo(), 0);
    }
}
