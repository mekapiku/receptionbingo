package info.mekapiku.android.ReceptionBingo.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import info.mekapiku.android.ReceptionBingo.R;
import info.mekapiku.android.ReceptionBingo.bean.BingoData;
import info.mekapiku.android.ReceptionBingo.page.BingoSlot;
import info.mekapiku.android.ReceptionBingo.page.HistoryList;

/**
 * Created by mitsuyasu on 13/10/18.
 */
public class BingoViewPager extends Fragment {

    private MyPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    private List<BingoData> cheatList;

    public BingoViewPager(List<BingoData> cheatList) {
        this.cheatList = cheatList;
    }

    public static Fragment createInstance(List<BingoData> cheatList) {
        return new BingoViewPager(cheatList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bingo_viewpager_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPagerAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager) getActivity().findViewById(R.id.bingo_view_pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(1);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final static int PAGE_SIZE = 2;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_SIZE;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return HistoryList.createInstance();
                case 1:
                    return BingoSlot.createInstance(cheatList);
                default:
                    break;
            }
            return null;
        }
    }
}
